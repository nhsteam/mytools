﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Ini;
using System.IO;
using System.Threading;
using System.Diagnostics;
//Sever update 2
namespace EVE_ST_GetLogs
{

    public partial class Form1 : Form
    {
        private string strDesLogFolder   = string.Empty;
        private string strSrcViewerLogFolder = string.Empty;
        private string strSrcServerLog1 = string.Empty;
        private string strSrcServerLog2 = string.Empty;
        private string strSrcServerLog3 = string.Empty;

        private const string HEADER_DIALOG = "EVE-ST_GetLogsSupporter";

        public Form1()
        {
            InitializeComponent();

            //Read config file
            string strPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            strPath = strPath.Substring(0, strPath.LastIndexOf('\\'));

            if (File.Exists(string.Format("{0}\\config.ini", strPath)) == false)
            {
                return;
            }

            IniFile objIni = new IniFile(string.Format("{0}\\config.ini", strPath));

            strSrcViewerLogFolder = objIni.IniReadValue("Info", "SrceViewerLogFolder");
            lblViewerLogsPath.Text = strSrcViewerLogFolder;

            strSrcServerLog1 = objIni.IniReadValue("Info", "SrcServerLog1");
            lblServer1LogsPath.Text = strSrcServerLog1;

            strSrcServerLog2 = objIni.IniReadValue("Info", "SrcServerLog2");
            lblServer2LogsPath.Text = strSrcServerLog2;

            strSrcServerLog3 = objIni.IniReadValue("Info", "SrcServerLog3");
            lblServer3LogsPath.Text = strSrcServerLog3;

            strDesLogFolder = objIni.IniReadValue("Info", "DesLogFolder");
            lblDesLogsFolder.Text = strDesLogFolder;
        }

        /// <summary>
        /// btnGetLogs_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetLogs_Click(object sender, EventArgs e)
        {

            if (textBox1.Text == "")
            {
                MessageBox.Show("Please input prefix for Log folder name,,,", HEADER_DIALOG);
                return;
            }


            Control.CheckForIllegalCrossThreadCalls = false;
            //timer1.Enabled = true;
            Thread wThreadGetLogs = new Thread(delegate()
            {
                GetLogs();
            });
            wThreadGetLogs.Start();

        }

        /// <summary>
        /// GetLogs
        /// </summary>
        private void GetLogs()
        {
            //GEt log file
            string[] lstViewerLogFiles = Directory.GetFiles(strSrcViewerLogFolder);
            string[] lstServer1LogFiles = Directory.GetFiles(strSrcServerLog1);
            string[] lstServer2LogFiles = Directory.GetFiles(strSrcServerLog2);
            string[] lstServer3LogFiles = Directory.GetFiles(strSrcServerLog3);

            int maxCount = lstViewerLogFiles.Count() + lstServer1LogFiles.Count() + lstServer2LogFiles.Count() + lstServer3LogFiles.Count();

            progressBar1.Maximum = maxCount;
            progressBar1.Minimum = 0;
            progressBar1.Value = progressBar1.Minimum;
            progressBar1.Step = 1;


            string strDesLogFolderCurrent = Path.Combine(strDesLogFolder,textBox1.Text + "_" + DateTime.Now.ToString("yyyyMMddhhmmss"));//   string.Format(DateTime.Now.ToString(),"YYYY_MM_DD_HH_MM_SS"));
            string strDesViewerLogFolder = Path.Combine(strDesLogFolderCurrent,"ViewerLogs");
            //string strDesServerLogFolder = Path.Combine(strDesLogFolderCurrent, "ServerLogs");


            string strDes_Management_ServerLogFolder = Path.Combine(strDesLogFolderCurrent, @"ServerLogs\Management_ServerLogs");
            string strDes_Management_Services_ServerLogFolder = Path.Combine(strDesLogFolderCurrent, @"ServerLogs\Management_Services_ServerLogs");
            string strDes_Server_ServerLogFolder = Path.Combine(strDesLogFolderCurrent, @"ServerLogs\Server_ServerLogs");
            
            //Get EventLog

            //Get ViewerLog
            //Create Dir log for Viewer log
            Directory.CreateDirectory(strDesViewerLogFolder);
            
            //GEt log file
            //string[] lstViewerLogFiles = Directory.GetFiles(strSrcViewerLogFolder);
            foreach (string strSrcViewerLogFiles in lstViewerLogFiles)
            {
                string desViewerLogFile = Path.Combine(strDesViewerLogFolder, Path.GetFileName(strSrcViewerLogFiles));
                try
                {
                    lblFilCopying.Text =strSrcViewerLogFiles ;
                    File.Copy(strSrcViewerLogFiles, desViewerLogFile);
                    progressBar1.PerformStep();
                }
                catch (Exception)
                {
                    //do nothing
                }
            }

            //ServerLog1 (Log Server Management)
            //Create Dir log for serverlog
            Directory.CreateDirectory(strDes_Management_ServerLogFolder);
            //GEt log file
            //string[] lstServer1LogFiles = Directory.GetFiles(strSrcServerLog1);
            foreach (string strSrcServer1LogFile in lstServer1LogFiles)
            {
                string desServer1LogFile = Path.Combine(strDes_Management_ServerLogFolder, Path.GetFileName(strSrcServer1LogFile));
                try
                {
                    lblFilCopying.Text = strSrcServer1LogFile;
                    File.Copy(strSrcServer1LogFile, desServer1LogFile);
                    progressBar1.PerformStep();
                }
                catch (Exception)
                {
                    //do nothing
                }
            }

            //ServerLog2 (Log Server Viewer)
            Directory.CreateDirectory(strDes_Management_Services_ServerLogFolder);
            //string[] lstServer2LogFiles = Directory.GetFiles(strSrcServerLog2);
            foreach (string strSrcServer2LogFile in lstServer2LogFiles)
            {
                string desServerLog2File = Path.Combine(strDes_Management_Services_ServerLogFolder, Path.GetFileName(strSrcServer2LogFile));
                try
                {
                    lblFilCopying.Text = strSrcServer2LogFile;
                    File.Copy(strSrcServer2LogFile, desServerLog2File);
                    progressBar1.PerformStep();
                }
                catch (Exception)
                {
                    //do nothing
                }
            }



            //ServerLog3 (Log Management)
            Directory.CreateDirectory(strDes_Server_ServerLogFolder);
            //string[] lstServer3LogFiles = Directory.GetFiles(strSrcServerLog3);
            foreach (string strSrcServer3LogFile in lstServer3LogFiles)
            {
                string desServerLog3File = Path.Combine(strDes_Server_ServerLogFolder, Path.GetFileName(strSrcServer3LogFile));
                try
                {
                    lblFilCopying.Text = strSrcServer3LogFile;
                    File.Copy(strSrcServer3LogFile, desServerLog3File);
                    progressBar1.PerformStep();
                }
                catch (Exception)
                {
                    //do nothing
                }
            }

            lblFilCopying.Text = "";
            
            MessageBox.Show("Finish!!!!!!!", HEADER_DIALOG);
            progressBar1.Value = progressBar1.Minimum;

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnGetLogs_Click(sender, e);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnGoto_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(strDesLogFolder);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

       


    }
}
