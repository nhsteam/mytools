﻿namespace EVE_ST_GetLogs
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGetLogs = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblViewerLogsPath = new System.Windows.Forms.Label();
            this.lblServer1LogsPath = new System.Windows.Forms.Label();
            this.lblServer2LogsPath = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblDesLogsFolder = new System.Windows.Forms.Label();
            this.lblServer3LogsPath = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label4 = new System.Windows.Forms.Label();
            this.lblFilCopying = new System.Windows.Forms.Label();
            this.btnGoto = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnGetLogs
            // 
            this.btnGetLogs.Location = new System.Drawing.Point(476, 184);
            this.btnGetLogs.Name = "btnGetLogs";
            this.btnGetLogs.Size = new System.Drawing.Size(97, 28);
            this.btnGetLogs.TabIndex = 0;
            this.btnGetLogs.Text = "Get Logs";
            this.btnGetLogs.UseVisualStyleBackColor = true;
            this.btnGetLogs.Click += new System.EventHandler(this.btnGetLogs_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 25);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(670, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Prefix Log Folder";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "1) ViewerLogs:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "2) ServerLogs:";
            // 
            // lblViewerLogsPath
            // 
            this.lblViewerLogsPath.AutoSize = true;
            this.lblViewerLogsPath.Location = new System.Drawing.Point(108, 80);
            this.lblViewerLogsPath.Name = "lblViewerLogsPath";
            this.lblViewerLogsPath.Size = new System.Drawing.Size(87, 13);
            this.lblViewerLogsPath.TabIndex = 6;
            this.lblViewerLogsPath.Text = "ViewerLogsPath:";
            // 
            // lblServer1LogsPath
            // 
            this.lblServer1LogsPath.AutoSize = true;
            this.lblServer1LogsPath.Location = new System.Drawing.Point(108, 99);
            this.lblServer1LogsPath.Name = "lblServer1LogsPath";
            this.lblServer1LogsPath.Size = new System.Drawing.Size(87, 13);
            this.lblServer1LogsPath.TabIndex = 7;
            this.lblServer1LogsPath.Text = "ViewerLogsPath:";
            // 
            // lblServer2LogsPath
            // 
            this.lblServer2LogsPath.AutoSize = true;
            this.lblServer2LogsPath.Location = new System.Drawing.Point(108, 120);
            this.lblServer2LogsPath.Name = "lblServer2LogsPath";
            this.lblServer2LogsPath.Size = new System.Drawing.Size(87, 13);
            this.lblServer2LogsPath.TabIndex = 8;
            this.lblServer2LogsPath.Text = "ViewerLogsPath:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Logs will be get from :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 166);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "And output to:";
            // 
            // lblDesLogsFolder
            // 
            this.lblDesLogsFolder.AutoSize = true;
            this.lblDesLogsFolder.Location = new System.Drawing.Point(102, 166);
            this.lblDesLogsFolder.Name = "lblDesLogsFolder";
            this.lblDesLogsFolder.Size = new System.Drawing.Size(87, 13);
            this.lblDesLogsFolder.TabIndex = 12;
            this.lblDesLogsFolder.Text = "ViewerLogsPath:";
            // 
            // lblServer3LogsPath
            // 
            this.lblServer3LogsPath.AutoSize = true;
            this.lblServer3LogsPath.Location = new System.Drawing.Point(108, 140);
            this.lblServer3LogsPath.Name = "lblServer3LogsPath";
            this.lblServer3LogsPath.Size = new System.Drawing.Size(87, 13);
            this.lblServer3LogsPath.TabIndex = 13;
            this.lblServer3LogsPath.Text = "ViewerLogsPath:";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 242);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(670, 20);
            this.progressBar1.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 215);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Current Copying :";
            // 
            // lblFilCopying
            // 
            this.lblFilCopying.AutoSize = true;
            this.lblFilCopying.Location = new System.Drawing.Point(105, 215);
            this.lblFilCopying.Name = "lblFilCopying";
            this.lblFilCopying.Size = new System.Drawing.Size(0, 13);
            this.lblFilCopying.TabIndex = 16;
            // 
            // btnGoto
            // 
            this.btnGoto.Location = new System.Drawing.Point(579, 184);
            this.btnGoto.Name = "btnGoto";
            this.btnGoto.Size = new System.Drawing.Size(114, 28);
            this.btnGoto.TabIndex = 17;
            this.btnGoto.Text = "Goto Output Folder";
            this.btnGoto.UseVisualStyleBackColor = true;
            this.btnGoto.Click += new System.EventHandler(this.btnGoto_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 276);
            this.Controls.Add(this.btnGoto);
            this.Controls.Add(this.lblFilCopying);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.lblServer3LogsPath);
            this.Controls.Add(this.lblDesLogsFolder);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblServer2LogsPath);
            this.Controls.Add(this.lblServer1LogsPath);
            this.Controls.Add(this.lblViewerLogsPath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnGetLogs);
            this.Name = "Form1";
            this.Text = "EVE-ST_GetLogsSupporter_v1.0";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Enter += new System.EventHandler(this.btnGetLogs_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGetLogs;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblViewerLogsPath;
        private System.Windows.Forms.Label lblServer1LogsPath;
        private System.Windows.Forms.Label lblServer2LogsPath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblDesLogsFolder;
        private System.Windows.Forms.Label lblServer3LogsPath;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblFilCopying;
        private System.Windows.Forms.Button btnGoto;
    }
}

